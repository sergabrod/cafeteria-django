from django.db import models
from ckeditor.fields import RichTextField

class Page(models.Model):
    title = models.CharField(verbose_name="Título", max_length=200)
    """ 
    Para usar CKEditor en un campo de texto debemos primero instalarlo con pip
    pip install django-ckeditor
    Luego importar desde el model from ckeditor.fields import RichTextField
    y agregar el campo como lo hacemos aca con content
    creamos la migracion con python manage.py makemigrations app_name
    corremos la migracion con python manage.py migrate app_name
    esto no produce ningun impacto en la base de datos:
    python manage.py sqlmigrate pages 0004_auto_20200126_1446
    BEGIN;
    --
    -- Alter field content on page
    --
    COMMIT; 
    """

    content = RichTextField(verbose_name="Contenido")
    order = models.SmallIntegerField(verbose_name="Orden", default=0)
    created = models.DateTimeField(auto_now_add=True, verbose_name='Fecha de creación')
    updated = models.DateTimeField(auto_now=True, verbose_name='Fecha de modificación')

    class Meta:
        verbose_name = 'Página'    
        verbose_name_plural = 'Páginas'
        ordering = ['order', 'title']

    def __str__(self):
        return self.title
