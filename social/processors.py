from .models import Link

def context_dict(request):
    context_links = {}
    links = Link.objects.all()
    for link in links:
        context_links[link.key] = link.url
    return context_links