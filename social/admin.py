from django.contrib import admin
from .models import Link

class LinkAdmin(admin.ModelAdmin):
    readonly_fields = ('created', 'updated')

    # Con esta función restringimos en tiempo de ejecución que los usurarios
    # que pertenzcan al grupo Personal, no puedan editar el campo clave y el nombre
    # de la red social ya que lo usamos en el pie, solamente pueden modificar el link
    # para esto detectamos en tiempo de ejecucion si el usuario activo pertenece al
    # grupo Persona, si es asi retornamos la lista con los campos que no queremos 
    # que este grupo modifique y que no puedan ver la fecha de edición y creación
    def get_readonly_fields(self, request, obj=None):
        if request.user.groups.filter(name="Personal").exists():
            return ('key', 'name')
        else:
            return ('created', 'updated')

admin.site.register(Link, LinkAdmin)
