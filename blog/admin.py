from django.contrib import admin
from .models import Category, Post

# Register your models here.
class CategoryAdmin(admin.ModelAdmin):
    readonly_fields = ('created', 'updated')

class PostAdmin(admin.ModelAdmin):
    readonly_fields = ('created', 'updated')
    list_display = ('title', 'author', 'published', 'post_categories')
    ordering = ('author', 'published')
    # author__username indica a django que buscaremos por el campo username
    # del modelo relacionado author, lo mismo con categories__name
    search_fields = ('title', 'content', 'author__username', 'categories__name')
    date_hierarchy = 'published'
    list_filter = ('author__username', 'categories__name')

    # Para poder listar las categorías de cada posteo debemos definir un método
    # que luedo lo uso en list_display
    def post_categories(self, obj):
        # obj es el objeto que representa cada fila
        return ", ".join([cat.name for cat in obj.categories.all().order_by("name")])
    
    post_categories.short_description = "Categorías"

admin.site.register(Category, CategoryAdmin)
admin.site.register(Post, PostAdmin)